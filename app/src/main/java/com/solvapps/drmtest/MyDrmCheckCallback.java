package com.solvapps.drmtest;

import android.util.Log;
import android.widget.Toast;

import com.huawei.android.sdk.drm.DrmCheckCallback;

class MyDrmCheckCallback implements DrmCheckCallback {
    @Override
    public void onCheckSuccess() {
        //Authentication succeeded.
        Log.v("DRM Check :" , "ok");
    }
    @Override
    public void onCheckFailed() {
        //Authentication failed.
        Log.e("DRM Check :" , "wrong");
    }

}
