package com.solvapps.drmtest;

import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.huawei.android.sdk.drm.Drm;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity {

    //Copyright Protection ID
    private static final String DRM_ID = "890041000024105375";
    //Copyright Protection Public Key
    private static final String DRM_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnxectqffAnOPP6VtbCQQi7AS2Srzi8r66yQgQpoU9qnjlX1EwNTliYxh7UitqJorNvgv8emwzSsgLG9xmCmet/yGoTe7l0SbjLcEyOzVXu7uWWJCKxuhn2cY6jYHpLzCw1QaGdcoz2a+USy7zHxlfK1oSIRcgnOqL95BbVPCrkxY4gQ9xnK+pPtjwVCt+hEvTb8FPF/TlTtp7TZbJ7rN5HgR/O8D0TODcsRtZY8ANOQRElT5SMBXlPUKLl8kFuFcaHvIu43p1mxtFYfx+3eUy/Eu1aM7zMId2+eVneeMhXL8qmqIGeOy+PWDPmLIQvaQt2LeXJO+qdQxCNyz4tQB0QIDAQAB";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);

        // Authentication invoking method
        Drm.check(this, this.getPackageName(), DRM_ID, DRM_PUBLIC_KEY,new MyDrmCheckCallback());

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
